---
title: "Reading"
description: "My reading list"
draft: false
tags: ["reading"]
date: 2020-10-21
math: false
author: "Julian Nordt"
---

## Reading

I like a wide variety on non-fictional literature (economics, business, biology, philosophy). On fictional books, I tend to concentrate on sci-fi. The list is broadly seprated into sci-fi and non-fiction. The list has no particular order and is incomplete.

*Regular short reads*

* [Netinterest](https://www.netinterest.email/) - by Marc Rubinstein - currently this is one of my favorite finance reads. Great mix of new topics with a detailed, yet strategic and humorous analysis. 

* [Gwern](https://www.gwern.net/index)  - Essays. 


*Books*

* Crucial Conversations
* Zero to One
* The score takes care of itself
* High Output Management
* Solution selling
* The Challenger sale
* Thinking, Fast and Slow
* Algorithms to live by
* Marc Aurel: Self-reflections
* Outliers
* Finite and Infinite Games
* Deutsche Gesellschaftsgeschichte Fünfter Band
* The second Machine Age
* The supermodel and the Brillo Box
* Liar's poker
* Getting past NO
* The hard thing about hard things
* The Art spirit
* Reboot
* Turn the ship around
* The five dysfunctions of a team
* Expert political judgement, Phil Tetlock
* Metamagical Themas, Douglas Hofstaeter

*Sci-fi*

* Rainbows End
* A Fire Upon the Deep
* A Deepness in the Sky
* Exhalations
* Stories of your life and others
* Revelation Space
* Chasm City
* Redemption Ark
* Absolution Gap
* Pushing ice
* House of Suns
* Blue Remembered Earth
* On The Steel Breeze
* Poseidon's Wake
* Consider Phlebas
* The Player of Games
* Use of Weapons
* Surface Detail
* The Hydrogen Sonata
* Dune
* Dune Messiah
* Terra Ignota
