---
heading: ""
subheading: "Hi, I'm Julian Nordt"
shortintro: "I co-founded and currently work at ENWAY, where we build software that makes heavy machinery autonomous. My background is originally in Computer Science and Bioinformatics at ETH Zürich and University of Tuebingen. However, I have always had a keen interest in Business, Finance and Entrepreneurial Ventures."
pagestructure: "This page has three main sections:
(1) The about page which you are currently on, (2) the notes section in which I write down small scribbles on various topics and (3) the reading section which lists books and other mediums that I have recently read."
---
